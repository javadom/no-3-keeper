package craky.componentc;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTableHeaderUI;

import craky.util.UIResourceManager;
import craky.util.UIUtil;

public class CTableHeaderUI extends BasicTableHeaderUI
{
    private static final Image BG_IMAGE = UIResourceManager.getImage(UIResourceManager.KEY_TABLE_HEADER_DEFAULT_IMAGE);
    
    private static final Image DISABLED_BG_IMAGE = UIUtil.toBufferedImage(BG_IMAGE, 0.5f, null);
    
    public static ComponentUI createUI(JComponent header)
    {
        return new CTableHeaderUI();
    }

    protected void rolloverColumnUpdated(int oldColumn, int newColumn)
    {
        header.repaint(header.getHeaderRect(oldColumn));
        header.repaint(header.getHeaderRect(newColumn));
    }
    
    protected MouseInputListener createMouseInputListener()
    {
        return new CMouseInputHandler();
    }
    
    public void update(Graphics g, JComponent c)
    {
        Image image = c.isEnabled()? BG_IMAGE: DISABLED_BG_IMAGE;
        UIUtil.paintImage(g, image, new Insets(1, 1, 1, 1), new Rectangle(0, 0, c.getWidth(), c.getHeight() - 1), c);
        paint(g, c);
    }
    
    public int getRolloverColumn()
    {
        return super.getRolloverColumn();
    }
    
    protected void installDefaults()
    {}

    public class CMouseInputHandler extends MouseInputHandler
    {
        public void mousePressed(MouseEvent e)
        {
            if(SwingUtilities.isLeftMouseButton(e))
            {
                super.mousePressed(e);
            }
        }
    }
}