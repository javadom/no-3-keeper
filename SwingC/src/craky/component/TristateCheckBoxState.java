package craky.component;

public enum TristateCheckBoxState
{
    SELECTED, DESELECTED, NOTSPECIFIED
}