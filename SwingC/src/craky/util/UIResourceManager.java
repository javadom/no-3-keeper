package craky.util;

import java.awt.Color;
import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import craky.componentc.ImageBorder;

public class UIResourceManager
{
    private static final String IMAGE_DIR = "/images/";
    
    private static final Map<String, Color> colorMap = new HashMap<String, Color>();
    
    private static final Map<String, Border> borderMap = new HashMap<String, Border>();
    
    private static final Map<String, Image> imageMap = new HashMap<String, Image>();
    
    private static final Map<String, Icon> iconMap = new HashMap<String, Icon>();
    
    private static final String KEY_WHITE_COLOR = "WhiteColor";
    
    private static final String KEY_EMPTY_COLOR = "EmptyColor";
    
    public static final String KEY_TEXT_SELECTION_FOREGROUND = "TextSelectionForeground";
    
    public static final String KEY_TEXT_SELECTION_COLOR = "TextSelectionColor";
    
    public static final String KEY_TEXT_NON_EDITABLE_BACKGROUND = "TextNonEditableBackground";
    
    public static final String KEY_TEXT_DISABLED_BACKGROUND = "TextDisabledBackground";
    
    public static final String KEY_SCROLL_BAR_BORDER_COLOR = "ScrollBarBorderColor";
    
    public static final String KEY_SCROLL_BAR_BACKGROUND = "ScrollBarBackground";
    
    public static final String KEY_SLIDER_DISABLED_TICK_COLOR = "SliderDisabledTickColor";
    
    public static final String KEY_TREE_LINE_COLOR = "TreeLineColor";
    
    public static final String KEY_SLIDER_MINI_OUTER_COLOR = "SliderMiniOuterColor";
    
    public static final String KEY_SLIDER_MINI_BACKGROUND = "SliderMiniBackground";
    
    public static final String KEY_SLIDER_MINI_INNER_COLOR_1 = "SliderMiniInnerColor1";
    
    public static final String KEY_SLIDER_MINI_INNER_COLOR_2 = "SliderMiniInnerColor2";
    
    public static final String KEY_SLIDER_OUTER_COLOR_1 = "SliderOuterColor1";
    
    public static final String KEY_SLIDER_OUTER_COLOR_2 = "SliderOuterColor2";
    
    public static final String KEY_SLIDER_INNER_COLOR_1 = "SliderInnerColor1";
    
    public static final String KEY_SLIDER_INNER_COLOR_2 = "SliderInnerColor2";
    
    public static final String KEY_CALENDAR_DAY_BACKGROUND = "CalendarDayBackground";
    
    public static final String KEY_CALENDAR_WEEK_FOREGROUND = "CalendarWeekForeground";
    
    public static final String KEY_CALENDAR_WEEKEND_FOREGROUND = "CalendarWeekendForeground";
    
    public static final String KEY_COMBO_BOX_RENDERER_BORDER = "ComboBoxRendererBorder";
    
    public static final String KEY_COMBO_BOX_RENDERER_SELECTED_BORDER = "ComboBoxRendererSelectedBorder";
    
    public static final String KEY_COMBO_BOX_EDITOR_BORDER = "ComboBoxEditorBorder";
    
    public static final String KEY_COMBO_BOX_POPUP_BORDER = "ComboBoxPopupBorder";
    
    public static final String KEY_LIST_RENDERER_BORDER = "ListRendererBorder";
    
    public static final String KEY_TREE_RENDERER_BORDER = "TreeRendererBorder";
    
    public static final String KEY_TREE_EDITOR_BORDER = "TreeEditorBorder";
    
    public static final String KEY_SCROLL_TEXT_BORDER = "ScrollTextBorder";
    
    public static final String KEY_SPINNER_EDITOR_BORDER = "SpinnerEditorBorder";
    
    public static final String KEY_SPINNER_EDITOR_DISABLED_BORDER = "SpinnerEditorDisabledBorder";
    
    public static final String KEY_TABLE_EDITOR_BORDER = "TableEditorBorder";
    
    public static final String KEY_TREE_COLUMN_EDITOR_BORDER = "TreeColumnEditorBorder";
    
    public static final String KEY_TEXT_ROLLOVER_BORDER = "TextRolloverBorder";
    
    public static final String KEY_TEXT_NON_EDITABLE_BORDER = "TextNonEditableBorder";
    
    public static final String KEY_TEXT_NON_EDITABLE_ROLLOVER_BORDER = "TextNonEditableRolloverBorder";
    
    public static final String KEY_TEXT_DISABLED_BORDER = "TextDisabledBorder";
    
    public static final String KEY_COMPOUND_TEXT_ROLLOVER_BORDER = "CompoundTextRolloverBorder";
    
    public static final String KEY_COMPOUND_TEXT_DISABLED_BORDER = "CompoundTextDisabledBorder";
    
    public static final String KEY_COMPOUND_TEXT_NON_EDITABLE_BORDER = "CompoundTextNonEditableBorder";
    
    public static final String KEY_COMPOUND_TEXT_NON_EDITABLE_ROLLOVER_BORDER = "CompoundTextNonEditableRolloverBorder";
    
    public static final String KEY_CALENDAR_DAY_BORDER = "CalendarDayBorder";
    
    public static final String KEY_CALENDAR_DAY_SELECTED_BORDER = "CalendarTodaySelectedBorder";
    
    public static final String KEY_COMBO_BOX_BUTTON_ICON = "ComboBoxButtonIcon";
    
    public static final String KEY_COMBO_BOX_BUTTON_DISABLED_ICON = "ComboBoxButtonDisabledIcon";
    
    public static final String KEY_SLIDER_MINI_THUMB_ICON_H = "SliderMiniThumbIconH";
    
    public static final String KEY_SLIDER_MINI_THUMB_ICON_V = "SliderMiniThumbIconV";
    
    public static final String KEY_SLIDER_THUMB_ICON_H = "SliderThumbIconH";
    
    public static final String KEY_SLIDER_THUMB_ICON_V = "SliderThumbIconV";
    
    public static final String KEY_SPINNER_PREVIOUS_ICON = "SpinnerPreviousIcon";
    
    public static final String KEY_SPINNER_PREVIOUS_DISABLED_ICON = "SpinnerPreviousDisabledIcon";
    
    public static final String KEY_SPINNER_NEXT_ICON = "SpinnerNextIcon";
    
    public static final String KEY_SPINNER_NEXT_DISABLED_ICON = "SpinnerNextDisabledIcon";
    
    public static final String KEY_TABLE_HEADER_UP_ICON = "TableHeaderUpIcon";
    
    public static final String KEY_TABLE_HEADER_DOWN_ICON = "TableHeaderDownIcon";
    
    public static final String KEY_TREE_NODE_DEFAULT_ICON = "TreeNodeDefaultIcon";
    
    public static final String KEY_TREE_EXPANDED_ICON = "TreeExpandedIcon";
    
    public static final String KEY_TREE_COLLAPSED_ICON = "TreeCollapsedIcon";
    
    public static final String KEY_SCROLL_PANE_LOWER_RIGHT_CORNER_ICON = "ScrollPaneLowerRightCornerIcon";
    
    public static final String KEY_CHECK_BOX_MENU_ITEM_SELECTED_ICON = "CheckBoxMenuItemSelectedIcon";
    
    public static final String KEY_MENU_ARROW_ICON = "MenuArrowIcon";
    
    public static final String KEY_MESSAGE_BOX_ERROR_ICON = "MessageBoxErrorIcon";
    
    public static final String KEY_MESSAGE_BOX_INFORMATION_ICON = "MessageBoxInfomationIcon";
    
    public static final String KEY_MESSAGE_BOX_WARNING_ICON = "MessageBoxWarningIcon";
    
    public static final String KEY_MESSAGE_BOX_QUESTION_ICON = "MessageBoxQuestionIcon";
    
    public static final String KEY_SELECTED_ITEM_BACKGROUND_IMAGE = "SelectedItemBackgroundImage";
    
    public static final String KEY_SELECTED_ITEM_DISABLED_BACKGROUND_IMAGE = "SelectedItemDisabledBackgroundImage";
    
    public static final String KEY_COMBO_BOX_RENDERER_BORDER_IMAGE = "ComboBoxRendererBorderImage";
    
    public static final String KEY_COMBO_BOX_RENDERER_BORDER_DISABLED_IMAGE = "ComboBoxRendererBorderDisabledImage";
    
    public static final String KEY_COMBO_BOX_BUTTON_IMAGE = "ComboBoxButtonImage";
    
    public static final String KEY_COMBO_BOX_BUTTON_ROLLOVER_IMAGE = "ComboBoxButtonRolloverImage";
    
    public static final String KEY_COMBO_BOX_BUTTON_PRESSED_IMAGE = "ComboBoxButtonPressedImage";
    
    public static final String KEY_SEPARATOR_IMAGE_H = "SeparatorImageH";
    
    public static final String KEY_SEPARATOR_IMAGE_V = "SeparatorImageV";
    
    public static final String KEY_SLIDER_BACKGROUND_IMAGE_H = "SliderBackgroundImageH";
    
    public static final String KEY_SLIDER_BACKGROUND_IMAGE_V = "SliderBackgroundImageV";
    
    public static final String KEY_SPINNER_PREVIOUS_IMAGE = "SpinnerPreviousImage";
    
    public static final String KEY_SPINNER_PREVIOUS_PRESSED_IMAGE = "SpinnerPreviousPressedImage";
    
    public static final String KEY_SPINNER_PREVIOUS_ROLLOVER_IMAGE = "SpinnerPreviousRolloverImage";
    
    public static final String KEY_SPINNER_PREVIOUS_DISABLED_IMAGE = "SpinnerPreviousDisabledImage";
    
    public static final String KEY_SPINNER_NEXT_IMAGE = "SpinnerNextImage";
    
    public static final String KEY_SPINNER_NEXT_PRESSED_IMAGE = "SpinnerNextPressedImage";
    
    public static final String KEY_SPINNER_NEXT_ROLLOVER_IMAGE = "SpinnerNextRolloverImage";
    
    public static final String KEY_SPINNER_NEXT_DISABLED_IMAGE = "SpinnerNextDisabledImage";
    
    public static final String KEY_TABLE_HEADER_DEFAULT_IMAGE = "TableHeaderDefaultImage";
    
    public static final String KEY_TABLE_HEADER_PRESSED_IMAGE = "TableHeaderPressedImage";
    
    public static final String KEY_TABLE_HEADER_ROLLOVER_IMAGE = "TableHeaderRolloverImage";
    
    public static final String KEY_TABLE_HEADER_SPLIT_IMAGE = "TableHeaderSplitImage";
    
    public static final String KEY_HEADER_PANE_BACKGROUND_IMAGE = "HeaderPaneBackgroundImage";
    
    public static final String KEY_SCROLL_TABLE_SHOW_MENU_BUTTON_IMAGE = "ScrollTableShowMenuButtonImage";
    
    public static final String KEY_SCROLL_TABLE_SHOW_MENU_BUTTON_ROLLOVER_IMAGE = "ScrollTableShowMenuButtonRolloverImage";
    
    public static final String KEY_SCROLL_TABLE_SHOW_MENU_BUTTON_PRESSED_IMAGE = "ScrollTableShowMenuButtonPressedImage";
    
    public static final String KEY_SCROLL_BAR_UP_IMAGE = "ScrollBarUpImage";

    public static final String KEY_SCROLL_BAR_UP_ROLLOVER_IMAGE = "ScrollBarUpRolloverImage";

    public static final String KEY_SCROLL_BAR_UP_PRESSED_IMAGE = "ScrollBarUpPressedImage";

    public static final String KEY_SCROLL_BAR_DOWN_IMAGE = "ScrollBarDownImage";

    public static final String KEY_SCROLL_BAR_DOWN_ROLLOVER_IMAGE = "ScrollBarDownRolloverImage";

    public static final String KEY_SCROLL_BAR_DOWN_PRESSED_IMAGE = "ScrollBarDownPressedImage";

    public static final String KEY_SCROLL_BAR_RIGHT_IMAGE = "ScrollBarRightImage";

    public static final String KEY_SCROLL_BAR_RIGHT_ROLLOVER_IMAGE = "ScrollBarRightRolloverImage";

    public static final String KEY_SCROLL_BAR_RIGHT_PRESSED_IMAGE = "ScrollBarRightPressedImage";

    public static final String KEY_SCROLL_BAR_LEFT_IMAGE = "ScrollBarLeftImage";

    public static final String KEY_SCROLL_BAR_LEFT_ROLLOVER_IMAGE = "ScrollBarLeftRolloverImage";

    public static final String KEY_SCROLL_BAR_LEFT_PRESSED_IMAGE = "ScrollBarLeftPressedImage";

    public static final String KEY_SCROLL_BAR_V_IMAGE = "ScrollBarVImage";

    public static final String KEY_SCROLL_BAR_V_ROLLOVER_IMAGE = "ScrollBarVRolloverImage";

    public static final String KEY_SCROLL_BAR_V_PRESSED_IMAGE = "ScrollBarVPressedImage";

    public static final String KEY_SCROLL_BAR_H_IMAGE = "ScrollBarHImage";

    public static final String KEY_SCROLL_BAR_H_ROLLOVER_IMAGE = "ScrollBarHRolloverImage";

    public static final String KEY_SCROLL_BAR_H_PRESSED_IMAGE = "ScrollBarHPressedImage";
    
    public static final String KEY_TABBED_PANE_BACDGROUND_IMAGE = "TabbedPaneBackgroundImage";
    
    public static final String KEY_TABBED_PANE_ROLLOVER_IMAGE = "TabbedPaneRolloverImage";
    
    public static final String KEY_TABBED_PANE_PRESSED_IMAGE = "TabbedPanePressedImage";
    
    public static final String KEY_TABBED_PANE_SPLIT_H_IMAGE = "TabbedPaneSplitHImage";
    
    public static final String KEY_TABBED_PANE_SPLIT_V_IMAGE = "TabbedPaneSplitVImage";
    
    public static final String KEY_TABBED_PANE_PREVIOUS_IMAGE = "TabbedPanePreviousImage";
    
    public static final String KEY_TABBED_PANE_PREVIOUS_ROLLOVER_IMAGE = "TabbedPanePreviousRolloverImage";
    
    public static final String KEY_TABBED_PANE_PREVIOUS_PRESSED_IMAGE = "TabbedPanePreviousPressedImage";
    
    public static final String KEY_TABBED_PANE_PREVIOUS_DISABLED_IMAGE = "TabbedPanePreviousDisabledImage";
    
    public static final String KEY_TABBED_PANE_NEXT_IMAGE = "TabbedPaneNextImage";
    
    public static final String KEY_TABBED_PANE_NEXT_ROLLOVER_IMAGE = "TabbedPaneNextRolloverImage";
    
    public static final String KEY_TABBED_PANE_NEXT_PRESSED_IMAGE = "TabbedPaneNextPressedImage";
    
    public static final String KEY_TABBED_PANE_NEXT_DISABLED_IMAGE = "TabbedPaneNextDisabledImage";
    
    public static final String KEY_MENU_BACKGROUND_IMAGE = "MenuBackgroundImage";
    
    public static final String KEY_MENU_NO_SHADOW_BACKGROUND_IMAGE = "MenuNoShadowBackgroundImage";
    
    public static final String KEY_MENU_ITEM_BACKGROUND_IMAGE = "MenuItemBackgroundImage";
    
    public static final String KEY_CALENDAR_BACKGROUND_IMAGE = "CalendarBackgroundImage";
    
    public static final String KEY_CALENDAR_CLOSE_ROLLOVER_IMAGE = "CalendarCloseRolloverImage";
    
    public static final String KEY_CALENDAR_CLOSE_PRESSED_IMAGE = "CalendarClosePressedImage";
    
    public static final String KEY_CALENDAR_CLOSE_IMAGE = "CalendarCloseImage";
    
    public static final String KEY_CALENDAR_NEXT_MONTH_ROLLOVER_IMAGE = "CalendarNextMonthRolloverImage";
    
    public static final String KEY_CALENDAR_NEXT_MONTH_PRESSED_IMAGE = "CalendarNextMonthPressedImage";
    
    public static final String KEY_CALENDAR_NEXT_MONTH_IMAGE = "CalendarNextMonthImage";
    
    public static final String KEY_CALENDAR_NEXT_YEAR_ROLLOVER_IMAGE = "CalendarNextYearRolloverImage";
    
    public static final String KEY_CALENDAR_NEXT_YEAR_PRESSED_IMAGE = "CalendarNextYearPressedImage";
    
    public static final String KEY_CALENDAR_NEXT_YEAR_IMAGE = "CalendarNextYearImage";
    
    public static final String KEY_CALENDAR_PREVIOUS_MONTH_ROLLOVER_IMAGE = "CalendarPreviousMonthRolloverImage";
    
    public static final String KEY_CALENDAR_PREVIOUS_MONTH_PRESSED_IMAGE = "CalendarPreviousMonthPressedImage";
    
    public static final String KEY_CALENDAR_PREVIOUS_MONTH_IMAGE = "CalendarPreviousMonthImage";
    
    public static final String KEY_CALENDAR_PREVIOUS_YEAR_ROLLOVER_IMAGE = "CalendarPreviousYearRolloverImage";
    
    public static final String KEY_CALENDAR_PREVIOUS_YEAR_PRESSED_IMAGE = "CalendarPreviousYearPressedImage";
    
    public static final String KEY_CALENDAR_PREVIOUS_YEAR_IMAGE = "CalendarPreviousYearImage";
    
    public static final String KEY_CALENDAR_COMBO_BOX_BUTTON_IMAGE = "CalendarComboBoxButtonImage";
    
    public static final String KEY_CALENDAR_COMBO_BOX_BUTTON_ROLLOVER_IMAGE = "CalendarComboBoxButtonRolloverImage";
    
    public static final String KEY_CALENDAR_COMBO_BOX_BUTTON_PRESSED_IMAGE = "CalendarComboBoxButtonPressedImage";
    
    public static final String KEY_WINDOW_CLOSE_IMAGE = "WindowCloseImage";
    
    public static final String KEY_WINDOW_CLOSE_ROLLOVER_IMAGE = "WindowCloseRolloverImage";
    
    public static final String KEY_WINDOW_CLOSE_PRESSED_IMAGE = "WindowClosePressedImage";
    
    public static final String KEY_WINDOW_MAX_IMAGE = "WindowMaxImage";
    
    public static final String KEY_WINDOW_MAX_ROLLOVER_IMAGE = "WindowMaxRolloverImage";
    
    public static final String KEY_WINDOW_MAX_PRESSED_IMAGE = "WindowMaxPressedImage";
    
    public static final String KEY_WINDOW_RESTORE_IMAGE = "WindowRestoreImage";
    
    public static final String KEY_WINDOW_RESTORE_ROLLOVER_IMAGE = "WindowRestoreRolloverImage";
    
    public static final String KEY_WINDOW_RESTORE_PRESSED_IMAGE = "WindowRestorePressedImage";
    
    public static final String KEY_WINDOW_MIN_IMAGE = "WindowMinImage";
    
    public static final String KEY_WINDOW_MIN_ROLLOVER_IMAGE = "WindowMinRolloverImage";
    
    public static final String KEY_WINDOW_MIN_PRESSED_IMAGE = "WindowMinPressedImage";
    
    public static final String KEY_WINDOW_BACKGROUND_IMAGE = "WindowBackgroundImage";
    
    public static final String KEY_WINDOW_TITLE_IMAGE = "WindowTitleImage";
    
    static
    {
        putColor(KEY_EMPTY_COLOR, new Color(0, 0, 0, 0));
        putColor(KEY_WHITE_COLOR, new Color(254, 255, 255));
        putColor(KEY_TEXT_DISABLED_BACKGROUND, new Color(250, 250, 249));
        putColor(KEY_TEXT_SELECTION_FOREGROUND, getColor(KEY_WHITE_COLOR));
        putColor(KEY_TEXT_SELECTION_COLOR, new Color(49, 106, 197));
        putColor(KEY_TEXT_NON_EDITABLE_BACKGROUND, new Color(235, 235, 235));
        putColor(KEY_SCROLL_BAR_BORDER_COLOR, new Color(226, 238, 243));
        putColor(KEY_SCROLL_BAR_BACKGROUND, new Color(235, 243, 246));
        putColor(KEY_SLIDER_DISABLED_TICK_COLOR, Color.GRAY);
        putColor(KEY_TREE_LINE_COLOR, new Color(172, 168, 153));
        putColor(KEY_SLIDER_OUTER_COLOR_1, new Color(157, 199, 240));
        putColor(KEY_SLIDER_OUTER_COLOR_2, new Color(154, 235, 171));
        putColor(KEY_SLIDER_INNER_COLOR_1, new Color(76, 181, 237));
        putColor(KEY_SLIDER_INNER_COLOR_2, new Color(53, 215, 89));
        putColor(KEY_SLIDER_MINI_OUTER_COLOR, new Color(78, 117, 160));
        putColor(KEY_SLIDER_MINI_BACKGROUND, Color.WHITE);
        putColor(KEY_SLIDER_MINI_INNER_COLOR_1, new Color(73, 239, 54));
        putColor(KEY_SLIDER_MINI_INNER_COLOR_2, new Color(238, 255, 71));
        putColor(KEY_CALENDAR_DAY_BACKGROUND, new Color(168, 219, 255));
        putColor(KEY_CALENDAR_WEEK_FOREGROUND, new Color(44,74, 137));
        putColor(KEY_CALENDAR_WEEKEND_FOREGROUND, new Color(255, 128, 128));
        
        putBorder(KEY_COMBO_BOX_RENDERER_BORDER, new EmptyBorder(0, 5, 0, 0));
        putBorder(KEY_COMBO_BOX_RENDERER_SELECTED_BORDER, new EmptyBorder(0, 3, 0, 0));
        putBorder(KEY_COMBO_BOX_EDITOR_BORDER, new EmptyBorder(2, 3, 0, 0));
        putBorder(KEY_COMBO_BOX_POPUP_BORDER, new CompoundBorder(new LineBorder(new Color(0, 147, 209)), new EmptyBorder(1, 1, 1, 1)));
        putBorder(KEY_LIST_RENDERER_BORDER, new EmptyBorder(0, 4, 0, 0));
        putBorder(KEY_TREE_RENDERER_BORDER, new EmptyBorder(0, 3, 0, 1));
        putBorder(KEY_TREE_EDITOR_BORDER, new CompoundBorder(new ImageBorder(getImageByName("border_rollover.png"), 2, 2, 2, 2),
                        new EmptyBorder(0, 1, 0, 1)));
        putBorder(KEY_SCROLL_TEXT_BORDER, new EmptyBorder(2, 3, 1, 1));
        putBorder(KEY_SPINNER_EDITOR_BORDER, new ImageBorder(getImageByName("border_editor_inner.png"), 2, 3, 0, 0));
        putBorder(KEY_SPINNER_EDITOR_DISABLED_BORDER, new ImageBorder(getImageByName("border_editor_inner_disabled.png"), 2, 3, 0, 0));
        putBorder(KEY_TABLE_EDITOR_BORDER, borderMap.get(KEY_TREE_EDITOR_BORDER));
        putBorder(KEY_TREE_COLUMN_EDITOR_BORDER, new CompoundBorder(new ImageBorder(getImageByName("border_rollover.png"), 2, 2, 2, 2),
                        new EmptyBorder(0, 0, 0, 1)));
        putBorder(KEY_TEXT_ROLLOVER_BORDER, new ImageBorder(getImageByName("border_rollover.png"), 5, 6, 3, 4));
        putBorder(KEY_TEXT_NON_EDITABLE_BORDER, new ImageBorder(getImageByName("border_noneditable.png"), 5, 6, 3, 4));
        putBorder(KEY_TEXT_NON_EDITABLE_ROLLOVER_BORDER, new ImageBorder(getImageByName("border_noneditable_rollover.png"), 5, 6, 3, 4));
        putBorder(KEY_TEXT_DISABLED_BORDER, new ImageBorder(getImageByName("border_disabled.png"), 5, 6, 3, 4));
        putBorder(KEY_COMPOUND_TEXT_ROLLOVER_BORDER, new ImageBorder(getImageByName("border_rollover.png"), 2, 2, 2, 2));
        putBorder(KEY_COMPOUND_TEXT_DISABLED_BORDER, new ImageBorder(getImageByName("border_disabled.png"), 2, 2, 2, 2));
        putBorder(KEY_COMPOUND_TEXT_NON_EDITABLE_BORDER, new ImageBorder(getImageByName("border_noneditable.png"), 2, 2, 2, 2));
        putBorder(KEY_COMPOUND_TEXT_NON_EDITABLE_ROLLOVER_BORDER, new ImageBorder(
                        getImageByName("border_noneditable_rollover.png"), 2, 2, 2, 2));
        putBorder(KEY_CALENDAR_DAY_BORDER, new EmptyBorder(2, 2, 2, 2));
        putBorder(KEY_CALENDAR_DAY_SELECTED_BORDER, new LineBorder(new Color(21, 88, 152), 2));
        
        putIcon(KEY_COMBO_BOX_BUTTON_ICON, getIconByName("combobox_arrow_button_icon.png"));
        putIcon(KEY_COMBO_BOX_BUTTON_DISABLED_ICON, getIconByName("combobox_arrow_button_disabled_icon.png"));
        putIcon(KEY_SLIDER_THUMB_ICON_H, getIconByName("slider_thumb_h.png"));
        putIcon(KEY_SLIDER_THUMB_ICON_V, getIconByName("slider_thumb_v.png"));
        putIcon(KEY_SLIDER_MINI_THUMB_ICON_H, getIconByName("slider_mini_thumb_h.png"));
        putIcon(KEY_SLIDER_MINI_THUMB_ICON_V, getIconByName("slider_mini_thumb_v.png"));
        putIcon(KEY_SPINNER_PREVIOUS_ICON, getIconByName("spinner_previous_button_icon.png"));
        putIcon(KEY_SPINNER_PREVIOUS_DISABLED_ICON, getIconByName("spinner_previous_button_disabled_icon.png"));
        putIcon(KEY_SPINNER_NEXT_ICON, getIconByName("spinner_next_button_icon.png"));
        putIcon(KEY_SPINNER_NEXT_DISABLED_ICON, getIconByName("spinner_next_button_disabled_icon.png"));
        putIcon(KEY_TABLE_HEADER_UP_ICON, getIconByName("table_arrow_up.png"));
        putIcon(KEY_TABLE_HEADER_DOWN_ICON, getIconByName("table_arrow_down.png"));
        putIcon(KEY_TREE_NODE_DEFAULT_ICON, getIconByName("tree_node.png"));
        putIcon(KEY_TREE_EXPANDED_ICON, getIconByName("tree_expanded.png"));
        putIcon(KEY_TREE_COLLAPSED_ICON, getIconByName("tree_collapsed.png"));
        putIcon(KEY_SCROLL_PANE_LOWER_RIGHT_CORNER_ICON, getIconByName("scrollpane_corner_l_r.png"));
        putIcon(KEY_CHECK_BOX_MENU_ITEM_SELECTED_ICON, getIconByName("checkbox_menuitem_selected.png"));
        putIcon(KEY_MENU_ARROW_ICON, getIconByName("menu_arrow.png"));
        putIcon(KEY_MESSAGE_BOX_ERROR_ICON, getIconByName("messagebox_error.png"));
        putIcon(KEY_MESSAGE_BOX_INFORMATION_ICON, getIconByName("messagebox_info.png"));
        putIcon(KEY_MESSAGE_BOX_WARNING_ICON, getIconByName("messagebox_warning.png"));
        putIcon(KEY_MESSAGE_BOX_QUESTION_ICON, getIconByName("messagebox_question.png"));
        
        putImage(KEY_SELECTED_ITEM_BACKGROUND_IMAGE, getImageByName("selected_item_background.png", true));
        putImage(KEY_SELECTED_ITEM_DISABLED_BACKGROUND_IMAGE, getImageByName("selected_item_disabled_background.png", true));
        putImage(KEY_COMBO_BOX_RENDERER_BORDER_IMAGE, getImageByName("border_editor_inner.png"));
        putImage(KEY_COMBO_BOX_RENDERER_BORDER_DISABLED_IMAGE, getImageByName("border_editor_inner_disabled.png"));
        putImage(KEY_COMBO_BOX_BUTTON_IMAGE, getImageByName("combobox_arrow_button_normal.png", true));
        putImage(KEY_COMBO_BOX_BUTTON_ROLLOVER_IMAGE, getImageByName("combobox_arrow_button_rollover.png", true));
        putImage(KEY_COMBO_BOX_BUTTON_PRESSED_IMAGE, getImageByName("combobox_arrow_button_pressed.png", true));
        putImage(KEY_SEPARATOR_IMAGE_H, getImageByName("separator_h.png"));
        putImage(KEY_SEPARATOR_IMAGE_V, getImageByName("separator_v.png"));
        putImage(KEY_SLIDER_BACKGROUND_IMAGE_H, getImageByName("slider_bg_h.png"));
        putImage(KEY_SLIDER_BACKGROUND_IMAGE_V, getImageByName("slider_bg_v.png"));
        putImage(KEY_SPINNER_PREVIOUS_IMAGE, getImageByName("spinner_previous_button_normal.png", true));
        putImage(KEY_SPINNER_PREVIOUS_PRESSED_IMAGE, getImageByName("spinner_previous_button_pressed.png", true));
        putImage(KEY_SPINNER_PREVIOUS_ROLLOVER_IMAGE, getImageByName("spinner_previous_button_rollover.png", true));
        putImage(KEY_SPINNER_PREVIOUS_DISABLED_IMAGE, getImageByName("spinner_previous_button_disabled.png", true));
        putImage(KEY_SPINNER_NEXT_IMAGE, getImageByName("spinner_next_button_normal.png", true));
        putImage(KEY_SPINNER_NEXT_PRESSED_IMAGE, getImageByName("spinner_next_button_pressed.png", true));
        putImage(KEY_SPINNER_NEXT_ROLLOVER_IMAGE, getImageByName("spinner_next_button_rollover.png", true));
        putImage(KEY_SPINNER_NEXT_DISABLED_IMAGE, getImageByName("spinner_next_button_disabled.png", true));
        putImage(KEY_HEADER_PANE_BACKGROUND_IMAGE, getImageByName("table_header_default.png", true));
        putImage(KEY_TABLE_HEADER_DEFAULT_IMAGE, imageMap.get(KEY_HEADER_PANE_BACKGROUND_IMAGE));
        putImage(KEY_TABLE_HEADER_PRESSED_IMAGE, getImageByName("table_header_press.png", true));
        putImage(KEY_TABLE_HEADER_ROLLOVER_IMAGE, getImageByName("table_header_rollover.png", true));
        putImage(KEY_TABLE_HEADER_SPLIT_IMAGE, getImageByName("table_header_split.png", true));
        putImage(KEY_SCROLL_TABLE_SHOW_MENU_BUTTON_IMAGE, imageMap.get(KEY_HEADER_PANE_BACKGROUND_IMAGE));
        putImage(KEY_SCROLL_TABLE_SHOW_MENU_BUTTON_ROLLOVER_IMAGE, imageMap.get(KEY_TABLE_HEADER_ROLLOVER_IMAGE));
        putImage(KEY_SCROLL_TABLE_SHOW_MENU_BUTTON_PRESSED_IMAGE, imageMap.get(KEY_TABLE_HEADER_PRESSED_IMAGE));
        putImage(KEY_SCROLL_BAR_UP_IMAGE, getImageByName("scrollbar_up.png", true));
        putImage(KEY_SCROLL_BAR_UP_ROLLOVER_IMAGE, getImageByName("scrollbar_up_r.png", true));
        putImage(KEY_SCROLL_BAR_UP_PRESSED_IMAGE, getImageByName("scrollbar_up_p.png", true));
        putImage(KEY_SCROLL_BAR_DOWN_IMAGE, getImageByName("scrollbar_down.png", true));
        putImage(KEY_SCROLL_BAR_DOWN_ROLLOVER_IMAGE, getImageByName("scrollbar_down_r.png", true));
        putImage(KEY_SCROLL_BAR_DOWN_PRESSED_IMAGE, getImageByName("scrollbar_down_p.png", true));
        putImage(KEY_SCROLL_BAR_RIGHT_IMAGE, getImageByName("scrollbar_right.png", true));
        putImage(KEY_SCROLL_BAR_RIGHT_ROLLOVER_IMAGE, getImageByName("scrollbar_right_r.png", true));
        putImage(KEY_SCROLL_BAR_RIGHT_PRESSED_IMAGE, getImageByName("scrollbar_right_p.png", true));
        putImage(KEY_SCROLL_BAR_LEFT_IMAGE, getImageByName("scrollbar_left.png", true));
        putImage(KEY_SCROLL_BAR_LEFT_ROLLOVER_IMAGE, getImageByName("scrollbar_left_r.png", true));
        putImage(KEY_SCROLL_BAR_LEFT_PRESSED_IMAGE, getImageByName("scrollbar_left_p.png", true));
        putImage(KEY_SCROLL_BAR_V_IMAGE, getImageByName("scrollbar_v.png", true));
        putImage(KEY_SCROLL_BAR_V_ROLLOVER_IMAGE, getImageByName("scrollbar_v_r.png", true));
        putImage(KEY_SCROLL_BAR_V_PRESSED_IMAGE, getImageByName("scrollbar_v_p.png", true));
        putImage(KEY_SCROLL_BAR_H_IMAGE, getImageByName("scrollbar_h.png", true));
        putImage(KEY_SCROLL_BAR_H_ROLLOVER_IMAGE, getImageByName("scrollbar_h_r.png", true));
        putImage(KEY_SCROLL_BAR_H_PRESSED_IMAGE, getImageByName("scrollbar_h_p.png", true));
        putImage(KEY_TABBED_PANE_BACDGROUND_IMAGE, getImageByName("tabbedpane_background.png"));
        putImage(KEY_TABBED_PANE_ROLLOVER_IMAGE, getImageByName("tabbedpane_rollover.png"));
        putImage(KEY_TABBED_PANE_PRESSED_IMAGE, getImageByName("tabbedpane_pressed.png"));
        putImage(KEY_TABBED_PANE_SPLIT_H_IMAGE, getImageByName("tabbedpane_seperator_h.png"));
        putImage(KEY_TABBED_PANE_SPLIT_V_IMAGE, getImageByName("tabbedpane_seperator_v.png"));
        putImage(KEY_TABBED_PANE_PREVIOUS_IMAGE, getImageByName("tabbedpane_previous_button.png", true));
        putImage(KEY_TABBED_PANE_PREVIOUS_ROLLOVER_IMAGE, getImageByName("tabbedpane_previous_button_rollover.png", true));
        putImage(KEY_TABBED_PANE_PREVIOUS_PRESSED_IMAGE, getImageByName("tabbedpane_previous_button_pressed.png", true));
        putImage(KEY_TABBED_PANE_PREVIOUS_DISABLED_IMAGE, getImageByName("tabbedpane_previous_button_disabled.png", true));
        putImage(KEY_TABBED_PANE_NEXT_IMAGE, getImageByName("tabbedpane_next_button.png", true));
        putImage(KEY_TABBED_PANE_NEXT_ROLLOVER_IMAGE, getImageByName("tabbedpane_next_button_rollover.png", true));
        putImage(KEY_TABBED_PANE_NEXT_PRESSED_IMAGE, getImageByName("tabbedpane_next_button_pressed.png", true));
        putImage(KEY_TABBED_PANE_NEXT_DISABLED_IMAGE, getImageByName("tabbedpane_next_button_disabled.png", true));
        putImage(KEY_MENU_BACKGROUND_IMAGE, getImageByName("menu_background.png"));
        putImage(KEY_MENU_NO_SHADOW_BACKGROUND_IMAGE, getImageByName("menu_no_shadow_background.png"));
        putImage(KEY_MENU_ITEM_BACKGROUND_IMAGE, getImageByName("menu_item_background.png", true));
        putImage(KEY_CALENDAR_BACKGROUND_IMAGE, getImageByName("calendar_background.png"));
        putImage(KEY_CALENDAR_CLOSE_ROLLOVER_IMAGE, getImageByName("calendar_close_rollover.png", true));
        putImage(KEY_CALENDAR_CLOSE_PRESSED_IMAGE, getImageByName("calendar_close_pressed.png", true));
        putImage(KEY_CALENDAR_CLOSE_IMAGE, getImageByName("calendar_close_normal.png", true));
        putImage(KEY_CALENDAR_NEXT_MONTH_ROLLOVER_IMAGE, getImageByName("calendar_nextmonth_rollover.png", true));
        putImage(KEY_CALENDAR_NEXT_MONTH_PRESSED_IMAGE, getImageByName("calendar_nextmonth_pressed.png", true));
        putImage(KEY_CALENDAR_NEXT_MONTH_IMAGE, getImageByName("calendar_nextmonth_normal.png", true));
        putImage(KEY_CALENDAR_NEXT_YEAR_ROLLOVER_IMAGE, getImageByName("calendar_nextyear_rollover.png", true));
        putImage(KEY_CALENDAR_NEXT_YEAR_PRESSED_IMAGE, getImageByName("calendar_nextyear_pressed.png", true));
        putImage(KEY_CALENDAR_NEXT_YEAR_IMAGE, getImageByName("calendar_nextyear_normal.png", true));
        putImage(KEY_CALENDAR_PREVIOUS_MONTH_ROLLOVER_IMAGE, getImageByName("calendar_premonth_rollover.png", true));
        putImage(KEY_CALENDAR_PREVIOUS_MONTH_PRESSED_IMAGE, getImageByName("calendar_premonth_pressed.png", true));
        putImage(KEY_CALENDAR_PREVIOUS_MONTH_IMAGE, getImageByName("calendar_premonth_normal.png", true));
        putImage(KEY_CALENDAR_PREVIOUS_YEAR_ROLLOVER_IMAGE, getImageByName("calendar_preyear_rollover.png", true));
        putImage(KEY_CALENDAR_PREVIOUS_YEAR_PRESSED_IMAGE, getImageByName("calendar_preyear_pressed.png", true));
        putImage(KEY_CALENDAR_PREVIOUS_YEAR_IMAGE, getImageByName("calendar_preyear_normal.png", true));
        putImage(KEY_CALENDAR_COMBO_BOX_BUTTON_IMAGE, getImageByName("calendar_button.png", true));
        putImage(KEY_CALENDAR_COMBO_BOX_BUTTON_ROLLOVER_IMAGE, imageMap.get(KEY_CALENDAR_COMBO_BOX_BUTTON_IMAGE));
        putImage(KEY_CALENDAR_COMBO_BOX_BUTTON_PRESSED_IMAGE, getImageByName("calendar_button_pressed.png", true));
        putImage(KEY_WINDOW_CLOSE_IMAGE, getImageByName("win_close_normal.png", true));
        putImage(KEY_WINDOW_CLOSE_ROLLOVER_IMAGE, getImageByName("win_close_rollover.png", true));
        putImage(KEY_WINDOW_CLOSE_PRESSED_IMAGE, getImageByName("win_close_pressed.png", true));
        putImage(KEY_WINDOW_MAX_IMAGE, getImageByName("win_max_normal.png", true));
        putImage(KEY_WINDOW_MAX_ROLLOVER_IMAGE, getImageByName("win_max_rollover.png", true));
        putImage(KEY_WINDOW_MAX_PRESSED_IMAGE, getImageByName("win_max_pressed.png", true));
        putImage(KEY_WINDOW_RESTORE_IMAGE, getImageByName("win_restore_normal.png", true));
        putImage(KEY_WINDOW_RESTORE_ROLLOVER_IMAGE, getImageByName("win_restore_rollover.png", true));
        putImage(KEY_WINDOW_RESTORE_PRESSED_IMAGE, getImageByName("win_restore_pressed.png", true));
        putImage(KEY_WINDOW_MIN_IMAGE, getImageByName("win_min_normal.png", true));
        putImage(KEY_WINDOW_MIN_ROLLOVER_IMAGE, getImageByName("win_min_rollover.png", true));
        putImage(KEY_WINDOW_MIN_PRESSED_IMAGE, getImageByName("win_min_pressed.png", true));
        putImage(KEY_WINDOW_BACKGROUND_IMAGE, getImageByName("window_background.png", true));
        putImage(KEY_WINDOW_TITLE_IMAGE, getImageByName("window_title.png"));
    }
    
    public static Color getEmptyColor()
    {
        return getColor(KEY_EMPTY_COLOR);
    }
    
    public static Color getWhiteColor()
    {
        return getColor(KEY_WHITE_COLOR);
    }
    
    public static Image getImageByName(String name, boolean createdByIcon)
    {
        if(createdByIcon)
        {
            return getIconByName(name).getImage();
        }
        else
        {
            return SwingResourceManager.getImage(UIResourceManager.class, IMAGE_DIR + name);
        }
    }
    
    public static Image getImageByName(String name)
    {
        return getImageByName(name, false);
    }
    
    public static ImageIcon getIconByName(String name)
    {
        return SwingResourceManager.getIcon(UIResourceManager.class, IMAGE_DIR + name);
    }
    
    public static void putColor(String key, Color color)
    {
        colorMap.put(key, color);
    }
    
    public static void putBorder(String key, Border border)
    {
        borderMap.put(key, border);
    }
    
    public static void putImage(String key, Image image)
    {
        imageMap.put(key, image);
    }
    
    public static void putIcon(String key, Icon icon)
    {
        iconMap.put(key, icon);
    }
    
    public static Color getColor(String key)
    {
        Color color = colorMap.get(key);
        return color == null? colorMap.get(KEY_EMPTY_COLOR): color;
    }
    
    public static Border getBorder(String key)
    {
        return borderMap.get(key);
    }
    
    public static Image getImage(String key)
    {
        return imageMap.get(key);
    }
    
    public static Icon getIcon(String key)
    {
        return iconMap.get(key);
    }
    
    public static Map<String, Color> getAllColors()
    {
        return colorMap;
    }
    
    public static Map<String, Image> getAllImages()
    {
        return imageMap;
    }
    
    public static Map<String, Icon> getAllIcons()
    {
        return iconMap;
    }
}